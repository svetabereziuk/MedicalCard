export const PATIENTS = {
    ADD_PATIENT: '[Patients] Add Patient',
    SET_PATIENTS: '[Patients] Set Patients',
    EDIT_PATIENT: '[Patients] Edit Patients',
    ADD_PATIENT_DIAGNOSE: '[Patients] Add Patient Diagnose',
    REMOVE_ACTIVE_MEDICATION: '[Patients] Remove Active Medication',
    UPDATE_MEDICATION: '[Patients] Update Medication',
    ADD_PATIENT_MEDICATION: '[Patients] Add Patient Medication',
    UPDATE_DIAGNOSES:  '[Patients] Update Diagnoses'
};
