import * as angular from 'angular';
import uiRouter from '@uirouter/angularjs';
import { appRoutes, ngReduxConfig } from './app.config';
import { AppComponent } from './app.component';
import { mainModule } from './main/main.module';
import  ngMaterial  = require('angular-material');
import 'angular-material/angular-material.css';
const ngRedux = require('ng-redux');

export const root = angular
  .module('app', [
    ngRedux,
    ngMaterial,
    uiRouter,    
    mainModule
])
  .component('app', AppComponent)
  .config(appRoutes)
  .config(ngReduxConfig)
  .name;
