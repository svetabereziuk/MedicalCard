import { Injectable } from 'angular-ts-decorators';

@Injectable()
export class  PatientsContainerService {
  static $inject = ['$http'];  
  constructor(private $http: ng.IHttpService) {}
  public getPatients() {
    return this.$http.get(`${ process.env.API_URL }patients`, { headers: { 'authorization': window.localStorage.getItem('LOCAL_TOKEN_KEY') } });     
  }

  public addPatient(patient) {
    this.$http.post(`${ process.env.API_URL }patients`, patient, { headers: { 'authorization': window.localStorage.getItem('LOCAL_TOKEN_KEY') } });
  }

  public getDefaultPaient() {
    return {
      id: 0,
      email: '',
      firstName: '',
      lastName: '',
      sex: '',
      birthData: '',
      deathData: '',
      business: '',
      city: '',
      state: '',
      street: '',
      house: '',
      zip: '',
      phone: '',
      workPhone: '',
      submissionDate: '',
      comments: '',
      medication: {
        "active": [],
        "history": []
      }
      ,
      diagnoses: {
        "active": [],
        "history": []
      }
    };
  }
}

