import { Component, Input, Output, OnInit, OnChanges } from 'angular-ts-decorators';
import { Inject } from 'angular-ts-decorators';
import './patients-container.component.scss';
import * as _ from 'lodash';
import { PatientsContainerService } from './patients-container.service';
import { Patient } from '../../components/add-patient/add-patient.interface';
import * as PatientActions from '../../../actions/patient.actions';

const templateUrl = require('./patients-container.component.html');

@Component({
  selector: 'patientsContainerComponent',
  template: templateUrl
})
export class PatientsContainerComponent implements OnInit, OnChanges {
  
  static $inject = ['PatientsContainerService', '$timeout', '$ngRedux','$scope'];
  patients;
  selectedPatient;
  filteredPatients;
  newPatient: Patient;
  message;
  id: string = "1";

  constructor(
    private patientsContainerService: PatientsContainerService,
    private timeout,
    private $ngRedux,
    $scope
    ) {
    const unsubscribe = $ngRedux.connect(this.mapStateToThis, PatientActions)(this);
    $scope.$on('$destroy', unsubscribe);
  };

  ngOnInit() {
    this.newPatient = this.patientsContainerService.getDefaultPaient();
    this.message = '';
    this.getPatients();           
  }

 

  selectPatient(index) {
    this.selectedPatient = this.patients.find(patient => patient.id === index);
    this.id = index;
  }
  

  changeFilter(res) { 
    this.timeout(() => {
      this.filteredPatients = res;
    });
  }

  createPatient({ patient }) {
    const isDuplicated = this.isDuplicated(this.patients, patient);
    this.message = isDuplicated ? 'This user is already exist' : 'Patient was successfully registered';
    !isDuplicated && this.addPatient(patient);
  }

  hideMessage() {
    this.message = '';
  }

  private mapStateToThis(state) {
    return {
      patients: state.patients
    };
  }
  ngOnChanges() {
    this.$ngRedux.subscribe(() => {
      let state = this.$ngRedux.getState();
      this.patients = state.patients;
      this.selectedPatient = this.patients.find(patient => patient.id === this.id);
    })

  }

  private getPatients() {
    if (_.isEmpty(this.patients)) {
      this.patientsContainerService.getPatients()
        .then(response => {
          this.$ngRedux.dispatch(PatientActions.setPatientsToState(response.data));
          this.setInitData();
        });
    } else {
      this.setInitData();
    }
  }

  private setInitData() {
    this.filteredPatients = [...this.patients];
    this.selectedPatient = this.patients[0];
  }

  private addPatient(patient) {
    const id = _.isEmpty(this.patients) ? 0 : _.last(this.patients)['id'] + 1;
    this.patientsContainerService.addPatient(patient);
    this.patients = this.patients.concat({ ...patient, id });
    this.filteredPatients = [...this.patients];
    this.newPatient = this.patientsContainerService.getDefaultPaient();
    this.$ngRedux.dispatch(PatientActions.addPatientToState({ ...patient, id }));
  }
  
  private isDuplicated(list, item) {
    const { firstName, lastName } = item;
    return _.some(list, { firstName, lastName });
  }  
}
