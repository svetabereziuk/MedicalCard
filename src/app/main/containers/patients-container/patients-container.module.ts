import * as angular from 'angular';
import { PatientsContainerComponent } from './patients-container.component';
import { PatientsContainerService } from './patients-container.service';
import { NgModule } from 'angular-ts-decorators';


@NgModule({
  id: 'PatientsContainerModule',
  declarations: [PatientsContainerComponent],
  providers: [
    {provide: 'PatientsContainerService', useClass: PatientsContainerService}
   ]
})
export class PatientsContainerModule {}
