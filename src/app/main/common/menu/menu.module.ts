import * as angular from 'angular';
import { MenuComponent } from './menu.component';
import { NgModule } from 'angular-ts-decorators';


@NgModule({
  declarations: [MenuComponent],
})
export class MenuModule {}
