import { Component, Input, OnChanges } from 'angular-ts-decorators';
import { PatientInfoInterface } from './patient-info.interface';
const templateUrl = require('./patient-info.component.html');
import './patient-info.component.scss';

@Component({
  selector: 'patient-info-component',
  template: templateUrl
 
})
export class PatientInfoComponent implements OnChanges  {
    @Input() patient;
    patientInfo: PatientInfoInterface;
    fullName;
    iconColor;
    initials;
    ngOnChanges(changes) {
        if (changes.patient.currentValue) {
          this.patient = changes.patient.currentValue;
          this.fullName = this.getFullName();
          this.initials = this.getInitials();
          this.iconColor = this.getIconColor();
        }
      }
    
      private getIconColor() {
      
        return (this.patient.sex === 'male') ? 'LightSkyBlue' : 'pink';
      }
    
      private getInitials() {
        const {firstName, lastName} = this.patient;
        const getFirstSymbol = (item) => item[0];
        return `${getFirstSymbol(firstName)}${getFirstSymbol(lastName)}`;
      }
    
      private getFullName() {
        return `${this.patient.firstName} ${this.patient.lastName}`;
      }
    
    
}
      

