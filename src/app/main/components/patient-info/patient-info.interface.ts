export interface PatientInfoInterface {
    readonly firstName: string;
    readonly lastName: string;
    readonly sex: string;
    readonly birthData: string;
    readonly deathData: string;
    readonly phone: string;
    readonly workPhone: string;
    readonly email: string;
    readonly house: string;
    readonly street: string;
    readonly city: string;
    readonly zip: string;
    readonly business: string;
}

