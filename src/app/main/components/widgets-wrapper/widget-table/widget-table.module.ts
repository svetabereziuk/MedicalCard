import { WidgetTableComponent } from './widget-table.component';
import { WidgetTableService } from './widget-table.service';
import { NgModule } from 'angular-ts-decorators';
import ngMaterial = require('angular-material');
import uiGrid = require('angular-ui-grid');
import 'angular-material/angular-material.css';
import 'angular-ui-grid/ui-grid.css';


@NgModule({
  id: 'WidgetTableModule',
  imports: [
    ngMaterial,
    uiGrid,
    'ui.grid.edit',
    'ui.grid.cellNav'
  ],
  declarations: [WidgetTableComponent],
  providers: [
    { provide: 'WidgetTableService', useClass: WidgetTableService }
  ]
})
export class WidgetTableModule { }
