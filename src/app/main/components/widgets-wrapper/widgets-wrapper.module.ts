import * as angular from 'angular';
import { NgModule } from 'angular-ts-decorators';
import { WidgetsWrapperComponent } from './widgets-wrapper.component';
import { WidgetTableModule } from './widget-table/widget-table.module';
import { widgetsSettingsModule } from './widgets-settings/widgets-settings.module';
import ngMaterial  = require('angular-material');
import uiGrid = require('angular-ui-grid');
import 'angular-material/angular-material.css';
import 'angular-ui-grid/ui-grid.css';
 
@NgModule({
  id: 'WidgetsWrapperModule',
  imports: [
    widgetsSettingsModule.name, 
    WidgetTableModule.name, 
    ngMaterial,
    uiGrid
  ],
  declarations: [WidgetsWrapperComponent]
})

export class WidgetsWrapperModule {}

