import * as angular from 'angular';
import { headerComponent } from './header.component';
import { LogoutModule } from './logout/logout.module';


export const headerModule = angular
  .module('app.main.header', [LogoutModule.name])
  .component('headerComponent', headerComponent)  
  .name;

