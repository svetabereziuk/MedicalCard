import './patient-edit.component.scss';
import { Component, Input} from 'angular-ts-decorators';

import * as PatientActions from '../../../actions/patient.actions';
import { SEXES } from './edit-patient.constant';
import * as angular from 'angular';
const templateUrl = require('./patient-edit.component.html');
@Component({
    selector: 'patient-edit-component',
    template: templateUrl
   
  })
export class PatientEditComponent {
  static $inject = ['$mdDialog','$ngRedux'];
  @Input() patient;
  patientEdit;
  sexes;
  constructor(    
        private $mdDialog: any,
        private $ngRedux
      ) {};
      ngOnInit() {
    
        this.sexes = this.getDropDownList(SEXES);
 
       
      }

      showPatientEditModal(ev) {
        this.$mdDialog.show({
          contentElement: '#editDialog',
          parent: angular.element(document.body),
          targetEvent: ev,
          clickOutsideToClose: true
          
     
        });
        this.patientEdit = angular.copy(this.patient);

      };
      getDropDownList(arr) {
        return arr.reduce((s, v) => s.concat({text : v}), []);
      }
    
      hidePatientEditModal() {
  
        this.$mdDialog.cancel();
   };
    
      savePatientEditModal() {
        
        this.$mdDialog.cancel();
        
        this.$ngRedux.dispatch(PatientActions.editPatient(this.patientEdit));
          this.$ngRedux.dispatch(PatientActions.editPatient(this.patientEdit));
       
       
      };
};
