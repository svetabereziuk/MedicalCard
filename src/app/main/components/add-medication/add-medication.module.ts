import * as angular from 'angular';
import { NgModule } from 'angular-ts-decorators';
import { AddMedicationComponent } from './add-medication.component';
import { AddMedicationService } from './add-medication.service';
import ngMaterial  = require('angular-material');

@NgModule({
  id: 'AddMedicationModule',
  imports: [
    ngMaterial
  ],
  declarations: [AddMedicationComponent],
  providers: [
    { provide: 'AddMedicationService', useClass: AddMedicationService },
  ]
})

export class AddMedicationModule {}
