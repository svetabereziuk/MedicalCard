import { Injectable } from 'angular-ts-decorators';

@Injectable()
export class AddMedicationService {
    static $inject = ['$http'];
    constructor(private $http: ng.IHttpService) {}
    
    getMedication() {
        return this.$http.get('medication.json');
    }

    getDoctors() {
        const doctors = ["Peter Klinsky", "Mark Zibert", "John Dell"];
        return doctors;
    }
    
    newMedication(){
        return {
            id: 0,
            date: new Date(),
            doctor: '',
            status: 'active',
            drugs: '',
            sig: ''
        }
    }
  
}