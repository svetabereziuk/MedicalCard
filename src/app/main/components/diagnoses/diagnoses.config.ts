diagnosesRoutes.$inject = ['$stateProvider'];

export function diagnosesRoutes($stateProvider: any) {
  $stateProvider.state('diagnoses', {
    url: '/diagnoses',
    component: 'DiagnosesComponent'
  });
}
