import * as _ from 'lodash';
import { Injectable } from 'angular-ts-decorators';

@Injectable()
export class DiagnosesTablesService {
  gridApi;
 
  getGridOptions() {
    return { 
      enableCellEdit: false,
      enableCellEditOnFocus: false,
      enableHorizontalScrollbar: false,
      enableVerticalScrollbar: true,
      rowHeight: 150,
      enableSorting: true,
      enableColumnMenus: false,
      modifierKeysToMultiSelectCells: true,
      keyDownOverrides: [{ keyCode: 39, ctrlKey: true }],
      showGridFooter: false,
    
    onRegisterApi: ( gridApi ) => {
      this.gridApi = gridApi;
      
    },
      columnDefs: this.getColumnDefs()
    };
  }
  
  getColumnDefs() {
    return [
      { name: 'name' },
      { name: 'diagnose', minWidth: 300},
      { name: 'date'},
      { name: 'resolved'},      
      { name: 'edit',        
        cellTemplate:`<diagnoses-tables-edit on-save-diagnose="grid.appScope.saveDiagnose($event)"></diagnoses-tables-edit>`
      }
    ];
  }  
}
