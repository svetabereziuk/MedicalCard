import * as angular from 'angular';
import { NgModule } from 'angular-ts-decorators';
import { DiagnosesTablesComponent } from './diagnoses-tables.component';
import { DiagnosesTablesService } from './diagnoses-tables.service';
import { DiagnosesTablesEditModule } from './diagnoses-tables-edit/diagnoses-tables-edit.module';
import ngMaterial = require('angular-material');
import uiGrid = require('angular-ui-grid');
import 'angular-material/angular-material.css';
import 'angular-ui-grid/ui-grid.css';


@NgModule({
  imports: [
    DiagnosesTablesEditModule,
    ngMaterial,
    uiGrid,
    'ui.grid',
    'ui.grid.edit',
    'ui.grid.cellNav',
    'ui.grid.expandable'
    
  ],
  declarations: [DiagnosesTablesComponent],
  providers: [
    { provide: 'DiagnosesTablesService', useClass: DiagnosesTablesService }
  ]
})

export class DiagnosesTablesModule {
}
