import * as angular from 'angular';
import { DiagnosesTablesEditService } from './diagnoses-tables-edit.service';
import { Component,  OnInit, Output } from 'angular-ts-decorators';
import { EditedDiagnose } from './diagnoses-tables-edit.interface';
import './diagnoses-tables-edit.component.scss';


const templateUrl = require('./diagnoses-tables-edit.component.html');

@Component({
    selector: 'diagnoses-tables-edit',
    template: templateUrl
})
export class DiagnosesTablesEditComponent implements OnInit {
  @Output() onSaveDiagnose;
 editedDiagnose: EditedDiagnose;

  static $inject = ['DiagnosesTablesEditService', '$mdDialog', '$mdMenu'];
    
    constructor(
      private DiagnosesTablesEditService: DiagnosesTablesEditService,
      private $mdDialog: any,
      private $mdMenu: any,
      
    ) {};

    ngOnInit() {
      this.editedDiagnose = this.DiagnosesTablesEditService.getEditedDiagnose();

    }
      
    onSubmit() {
      
      this.onSaveDiagnose({
        $event: {
          editedDiagnose: this.editedDiagnose
        }
      });
    }
      
    announceClick() {
      this.$mdDialog.show (
        this.$mdDialog.alert()
          .textContent('changes was saved')
          .ok('Ok')
      );
    };      
}
