import * as angular from 'angular';
import { NgModule } from 'angular-ts-decorators';
import { DiagnosesTablesEditComponent } from './diagnoses-tables-edit.component';
import { DiagnosesTablesEditService } from './diagnoses-tables-edit.service';
import ngMaterial = require('angular-material');


@NgModule({
  imports: [
    ngMaterial
  ],
  declarations: [DiagnosesTablesEditComponent],
  providers: [
    { provide: 'DiagnosesTablesEditService', useClass: DiagnosesTablesEditService }
  ]
})

export class DiagnosesTablesEditModule {
}
