import * as angular from 'angular';
import { NgModule } from 'angular-ts-decorators';
import { AddPatientComponent } from './add-patient.component';

@NgModule({
  id: 'AddPatientModule',
  declarations: [AddPatientComponent]
})

export class AddPatientModule {}
