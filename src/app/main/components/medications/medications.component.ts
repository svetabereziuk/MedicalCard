import './medications.component.scss';
import { TABS_DATA } from './medications.constant';
import * as _ from 'lodash';
import { Component, Input } from 'angular-ts-decorators';

const templateUrl = require('./medications.component.html');

@Component({
  selector: 'medications-component',
  template: templateUrl
 
})
export class MedicationsComponent {
  @Input() patient;
  properties;
  selected: string[];
  tabs;
  
  ngOnInit () {
    this.tabs = TABS_DATA;
  }    
}

