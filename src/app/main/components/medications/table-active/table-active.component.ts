import * as angular from 'angular';
import * as _ from 'lodash';
import './table-active.component.scss';
import { TableActiveService } from './table-active.service';
import { Component, Input, OnChanges, OnInit, Inject } from 'angular-ts-decorators';
import * as PatientActions from '../../../../actions/patient.actions';

const templateUrl = require('./table-active.component.html');

@Component({
    selector: 'table-active-component',
    template: templateUrl
})
export class TableActiveComponent implements OnChanges, OnInit {
  static $inject = ['TableActiveService', '$ngRedux', '$mdDialog'];
  @Input() patient;
  currentFocused: string;
  activeMeds;
  historyMeds;
  rowCol;
  currentFocusedRow;
  currentFocusedCol;
  gridOptions;
  historyItem;
  editedMeds;
  properties;
  id;

  constructor(private TableActiveService: TableActiveService,
    private $ngRedux) {};
    
    ngOnInit () {
      this.currentFocused = '';
    }
   
    ngOnChanges(changes) {
      if (changes.patient) {
        this.patient = Object.assign({}, this.patient);
      }
      this.gridOptions = this.TableActiveService.getGridOptions();
      this.gridOptions.appScopeProvider = this;
      this.gridOptions.data = this.patient.medication.active;
      this.activeMeds = this.patient.medication.active;
      this.historyMeds = this.patient.medication.history;
    }


    getFocus(){
      this.rowCol = this.TableActiveService.gridApi.cellNav.getFocusedCell();
      if (this.rowCol) {
          this.currentFocusedRow = this.rowCol.row.entity.id;
          this.currentFocusedCol = this.rowCol.col.colDef.name;
      }       
    }

    removeActiveMeds(row) {
      this.getFocus();
      this.id = this.currentFocusedRow;
      
      this.historyItem = _.filter(this.activeMeds, ['id', this.id])[0];
      this.activeMeds= _.reject(this.activeMeds, item => item.id === this.id);    
      this.historyMeds =_.concat(this.historyMeds, [this.historyItem]).sort((x,y) => y.id - x.id);
         
      this.patient = {...this.patient, medication: {...this.patient.medication, active: this.activeMeds, history: this.historyMeds}};
      this.$ngRedux.dispatch(PatientActions.removeActiveMedication(this.patient));
     
    }

    saveMeds({editedMeds}) {
      this.editedMeds = editedMeds;
      this.getFocus();
      this.id = this.currentFocusedRow;
      this.editedMeds.id = this.id;
     
      this.activeMeds = _.map(this.activeMeds, 
        item => {
          return (item.id === this.id) ? 
            item = Object.assign({}, this.editedMeds) : item;
      });
     
      this.patient = {...this.patient, medication: {...this.patient.medication, active: this.activeMeds}};
      this.$ngRedux.dispatch(PatientActions.updateMedication(this.patient));
    }
 }


