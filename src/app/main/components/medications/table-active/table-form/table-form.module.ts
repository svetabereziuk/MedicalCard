import * as angular from 'angular';
import { NgModule } from 'angular-ts-decorators';
import { TableFormComponent } from './table-form.component';
import { TableFormService } from './table-form.service';
import ngMaterial = require('angular-material');


@NgModule({
  imports: [
    ngMaterial
  ],
  declarations: [TableFormComponent],
  providers: [
    { provide: 'TableFormService', useClass: TableFormService }
  ]
})

export class TableFormModule {
}

