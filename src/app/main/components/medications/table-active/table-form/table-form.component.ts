import * as angular from 'angular';
import { TableFormService } from './table-form.service';
import { Component,  OnChanges, OnInit, Output } from 'angular-ts-decorators';
import { EditedMeds } from './table-form.interface';



const templateUrl = require('./table-form.component.html');

@Component({
    selector: 'table-form-component',
    template: templateUrl
})
export class TableFormComponent implements OnInit {
  @Output() onSaveMeds;
  editedMeds: EditedMeds;

  static $inject = ['TableFormService', '$mdDialog', '$mdMenu'];
    
    constructor(
      private TableFormService: TableFormService,
      private $mdDialog: any,
      private $mdMenu: any,
      
    ) {};

    ngOnInit() {
      this.editedMeds = this.TableFormService.getEditedMeds();  
    }
      
    onSubmit() {
      this.onSaveMeds({
        $event: {
          editedMeds: this.editedMeds
        }
      });
    }
      
    announceClick() {
      this.$mdDialog.show (
        this.$mdDialog.alert()
          .textContent('changes saved')
          .ok('Ok')
      );
    };      
}