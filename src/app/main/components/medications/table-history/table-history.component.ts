import * as angular from 'angular';
import * as _ from 'lodash';
import './table-history.component.scss';
import { TableHistoryService } from './table-history.service';
import * as PatientActions from '../../../../actions/patient.actions';
import { Component, Input, OnChanges, OnInit } from 'angular-ts-decorators';

const templateUrl = require('./table-history.component.html');

@Component({
  selector: 'table-history-component',
  template: templateUrl
})
export class TableHistoryComponent implements OnChanges, OnInit {
  @Input() patient;
  static $inject = ['TableHistoryService', '$ngRedux', '$mdDialog'];
  currentFocused;
  gridOptions;
  historyMeds;
  rowCol;
  currentFocusedRow;
  currentFocusedCol;
  editedMeds;
  activeMeds;
  id;
    
  constructor(
    private TableHistoryService: TableHistoryService,
    private $ngRedux) {};
    
  ngOnInit () {
    this.currentFocused = '';
  }
  

  ngOnChanges(changes) {
    if (changes.patient) {
      this.patient = Object.assign({}, this.patient);
    }
    this.gridOptions = this.TableHistoryService.getGridOptions();
    this.gridOptions.appScopeProvider = this;
    this.gridOptions.data = this.patient.medication.history;
    this.activeMeds = this.patient.medication.active;
    this.historyMeds = this.patient.medication.history;
  }

  getFocus(){
    this.rowCol = this.TableHistoryService.gridApi.cellNav.getFocusedCell(); 
    if (this.rowCol) {
         this.currentFocusedRow = this.rowCol.row.entity.id;
         this.currentFocusedCol = this.rowCol.col.colDef.name;
    }       
  }

  saveMeds({editedMeds}) {
    this.editedMeds = editedMeds;
    this.getFocus();
    this.id = this.currentFocusedRow;
    this.editedMeds.id = this.id;
   
    this.historyMeds = _.map(this.historyMeds, 
      item => {
        return (item.id === this.id) ? 
        item = Object.assign({}, this.editedMeds) : item;
    });
    this.patient = {...this.patient, medication: {...this.patient.medication, history: this.historyMeds}};
    this.$ngRedux.dispatch(PatientActions.updateMedication(this.patient));
  }
   
}

 


