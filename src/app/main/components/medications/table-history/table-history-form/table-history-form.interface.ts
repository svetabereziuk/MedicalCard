export interface EditedMeds {
  startDate: string,
  doctor: string,
  stopDate: string,
  by: string,
  drugs: string
}
 