import * as angular from 'angular';
import { NgModule } from 'angular-ts-decorators';
import { ShowPatientsListComponent } from './show-patients-list.component';
import { ShowPatientsListService } from './show-patients-list.service';

@NgModule({
  id: 'ShowPatientsListModule',
  declarations: [ShowPatientsListComponent],
  providers: [
    { provide: 'ShowPatientsListService', useClass: ShowPatientsListService } 
  ]
})

export class ShowPatientsListModule {

}
