import { USER } from '../constants/user';

export function addUser(user) {
  return {
    type: USER.ADD_USER,
    payload: user
  };
}

export function setUserToState(user) {
  return {
    type: USER.SET_USER,
    payload: user
  };
}
